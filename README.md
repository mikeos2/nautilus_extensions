## Miscellaneous Nautilus Extensions

This basically a dumping spot for my collected Nautilus python files. I found that swapping to the Gentoo Gnome 43 early meant a number of extension that worked with nautilus-python did not work or did not work exactly how I wanted them to work.

### openterm.py

This is my modified open-terminal.py file from the nautilus-python package. I started using new [Gnome Console](https://gitlab.gnome.org/GNOME/console) when I installed Gnome 43 and slowly got used to it. I wanted the option to select between Gnome Terminal and Gnome Console, so this modified extension. 

### clamtk-gnome.py

This is clamtk-gnome.py from the Gentoo app-antivirus/clamtk-6.14 package. It seems to be working now with Nautilus, but there is a delay before the scan starts.

### nautilus-copypath.py

This is a quick rehash of Lapushner code for nautilus 43/Gdk 4. The new clipboard stuff is a PITA but found an example:
[github.com/chr314/nautilus-copy-path](https://github.com/chr314/nautilus-copy-path)
It was a bit more than what I wanted, so I am using this minimal version.

