import gi
gi.require_version('Nautilus', '4.0')

import os
from urllib.parse import unquote
from gi.repository import Nautilus, GObject
from typing import List

"""
*******************************************************************************
   openterm.py      v1.0    03 Dec 2022
   
   My take on the open-terminal.py Nautilus extension with the current 
   Nautilus version 43.1 that    I am using. Currently the version of 
   nautilus-python in use is v4.0.
   
   My big change is adding a choice between gnome-terminal and gnome-console.
   
   Place in ./local/share/nautilus-python/extensions/ and restart nautilus.

*******************************************************************************

Nautilus.MenuProvider - If subclassed, Nautilus will request a list of 
Nautilus.MenuItem objects, which are then attached to various menus. Nautilus 
expects at least one of the following methods to be defined (or their *_full 
variants): get_file_items or get_background_items.

Nautilus.MenuProvider {
  get_file_items(files);
  get_file_items_full(provider, files);
  get_background_items(folder);
  get_background_items_full(provider, folder);

  Nautilus.menu_provider_emit_items_updated_signal(provider);
}

5 Dec 22: From nautilus-python gitlab.gnome.org: Nautilus only really supports 
          name, label and menu, so I removed tip.
          
6 Dec 22: Added back get_background_items now that I see what it was doing.        
"""


class OpenTerminalProvider(GObject.GObject, Nautilus.MenuProvider):
    def __init__(self):
        super().__init__()

    def _open_terminal(
            self,
            file: Nautilus.FileInfo,
            term
    ) -> None:

        # strip off file:// -- note: unquote might be left over from older version
        # file.get_uri() does not seem quoted
        filename = unquote(file.get_uri()[7:])

        # change to directory
        os.chdir(filename)

        # start the selected terminal
        os.system(term)

    def term_activate_cb(
            self,
            menu: Nautilus.MenuItem,
            file: Nautilus.FileInfo,
    ) -> None:
        self._open_terminal(file, "gnome-terminal")

    def cons_activate_cb(
            self,
            menu: Nautilus.MenuItem,
            file: Nautilus.FileInfo,
    ) -> None:
        self._open_terminal(file, "kgx")

    def get_file_items(
            self,
            files: List[Nautilus.FileInfo],
    ) -> List[Nautilus.MenuItem]:

        # ensure only one item is selected, return if not one
        if len(files) != 1:
            return []

        # set file to the selected item
        file = files[0]
        if not file.is_directory() or file.get_uri_scheme() != "file":
            return []

        # create the main menu item
        main_menuitem = Nautilus.MenuItem(
            name="OpenTerminalProvider::OpenTerm_main",
            label="Open in terminal",
        )

        # set up submenu and attach to main menu
        submenu = Nautilus.Menu()
        main_menuitem.set_submenu(submenu)

        # generate Gnome Console submenu entry and connect signal
        submenu_cons = Nautilus.MenuItem(
            name="OpenTerminalProvider::Console",
            label="Gnome Console",
        )
        submenu_cons.connect("activate", self.cons_activate_cb, file)

        # generate Gnome Terminal submenu entry and connect signal
        submenu_term = Nautilus.MenuItem(
            name="OpenTerminalProvider::Terminal",
            label="Gnome Terminal",
        )
        submenu_term.connect("activate", self.term_activate_cb, file)

        submenu.append_item(submenu_term)
        submenu.append_item(submenu_cons)

        return [main_menuitem, ]

    # not super happy with the way this background stuff works, but better than
    # nothing.
    def get_background_items(
        self,
        current_folder: Nautilus.FileInfo,
    ) -> List[Nautilus.MenuItem]:

        # create the main menu item
        main_menuitem = Nautilus.MenuItem(
            name="OpenTerminalProvider::bk_OpenTerm_main",
            label="Open in terminal",
        )

        # set up submenu and attach to main menu
        bk_submenu = Nautilus.Menu()
        main_menuitem.set_submenu(bk_submenu)

        # generate Gnome Console submenu entry and connect signal
        submenu_cons = Nautilus.MenuItem(
            name="OpenTerminalProvider::Console",
            label="Gnome Console",
        )
        submenu_cons.connect("activate", self.cons_activate_cb, current_folder)

        # generate Gnome Terminal submenu entry and connect signal
        submenu_term = Nautilus.MenuItem(
            name="OpenTerminalProvider::Terminal",
            label="Gnome Terminal",
        )
        submenu_term.connect("activate", self.term_activate_cb, current_folder)

        # attach both entries to the submenu
        bk_submenu.append_item(submenu_term)
        bk_submenu.append_item(submenu_cons)

        return [main_menuitem, ]
