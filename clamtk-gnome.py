import gi

gi.require_version('Nautilus', '4.0')

from gi.repository import Nautilus, GObject
from typing import List

# ClamTk, copyright (C) 2004-2021 Dave M, Tord Dellsén
#
# This file is part of ClamTk
# (https://gitlab.com/dave_m/clamtk/wikis/Home)
#
# ClamTk is free software; you can redistribute it and/or modify it
# under the terms of either:
#
# a) the GNU General Public License as published by the Free Software
# Foundation; either version 1, or (at your option) any later version, or
#
# b) the "Artistic License".

import locale
import subprocess

locale.setlocale(locale.LC_ALL, "")

import gettext

gettext.bindtextdomain("clamtk", "/usr/share/locale")
gettext.textdomain("clamtk")
_ = gettext.gettext


class OpenTerminalExtension(GObject.GObject, Nautilus.MenuProvider):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _open_scanner(
            self,
            file: Nautilus.FileInfo,
    ) -> None:
        filename = file.get_location().get_path()
        # - file is of type nautiuls-vsf-file
        # https://github.com/GNOME/nautilus/blob/master/src/nautilus-file.h
        # which inherits from nautilus-file
        # https://github.com/GNOME/nautilus/blob/master/src/nautilus-vfs-file.h
        # - get_location returns a GFile
        # https://developer.gnome.org/gio/stable/GFile.html
        # which has the get_path function which returns the absolute path as a string

        cmd = ["clamtk", filename]
        subprocess.Popen(cmd)

    def menu_activate_cb(
            self,
            menu: Nautilus.MenuItem,
            file: Nautilus.FileInfo,
    ) -> None:
        print("menu_activate_cb", file)
        self._open_scanner(file)

    def menu_background_activate_cb(
            self,
            menu: Nautilus.MenuItem,
            file: Nautilus.FileInfo,
    ) -> None:
        print("bg_activate_cb", file)
        self._open_scanner(file)

    def get_file_items(
            self,
            files: List[Nautilus.FileInfo],
    ) -> List[Nautilus.MenuItem]:
        if len(files) != 1:
            return

        file = files[0]

        # create the main menu item
        main_menuitem = Nautilus.MenuItem(
            name="NautilusPython::openscanner",
            label=_("ClamScan file ..."),
        )
        main_menuitem.connect("activate", self.menu_activate_cb, file)

        return [main_menuitem, ]

    def get_background_items(
            self,
            files: List[Nautilus.FileInfo],
    ) -> List[Nautilus.MenuItem]:
        # create the main menu item
        main_menuitem = Nautilus.MenuItem(
            name="NautilusPython::openscanner_directory",
            label=_("ClamScan directory ..."),
        )
        main_menuitem.connect("activate", self.menu_background_activate_cb, files)

        return [main_menuitem, ]
