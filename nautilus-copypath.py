import gi

gi.require_version('Gdk', '4.0')
gi.require_version('Nautilus', '4.0')

from gi.repository import Nautilus, GObject, Gdk
from typing import List

# MKG 17 Dec 2022 - a quick rehash of Lapushner code for nautilus 43/Gdk 4
# The new clipboard stuff is a PITA but found an example:
# https://github.com/chr314/nautilus-copy-path
#
# ----------------------------------------------------------------------------------------
# nautilus-copypath - Quickly copy file paths to the clipboard from Nautilus.
# Copyright (C) Ronen Lapushner 2017-2018.
# Distributed under the GPL-v3+ license. See LICENSE for more information
# ----------------------------------------------------------------------------------------


class CopyPathExtension(GObject.GObject, Nautilus.MenuProvider):
    def __init__(self, *args, **kwargs):
        # Initialize clipboard
        super().__init__(*args, **kwargs)

        # Gets the default GdkDisplay
        self.default_display = Gdk.Display.get_default()
        self.clipboard = self.default_display.get_clipboard()

    def __sanitize_path(self, path):
        # Replace spaces and parenthesis with their Linux-compatible equivalents.
        return path.replace(' ', '\\ ').replace('(', '\\(').replace(')', '\\)')

    def __copy_files_path(
            self,
            menu: Nautilus.MenuItem,
            files: Nautilus.FileInfo,
    ) -> None:

        pathstr = None

        # Get the paths for all the files.
        # Also, strip any protocol headers, if required.
        paths = [self.__sanitize_path(fileinfo.get_location().get_path())
                 for fileinfo in files]

        # Append to the path string
        if len(files) > 1:
            pathstr = '\n'.join(paths)
        elif len(files) == 1:
            pathstr = paths[0]

        # Set clipboard text
        if pathstr is not None:
            self.clipboard.set(pathstr)

    def __copy_dir_path(
            self,
            menu: Nautilus.MenuItem,
            path: Nautilus.FileInfo,
    ) -> None:

        if path is not None:
            pathstr = self.__sanitize_path(path.get_location().get_path())
            self.clipboard.set(pathstr)

    # If there are many items to copy, change the label
    # to reflect that.
    def get_file_items(
            self,
            files: List[Nautilus.FileInfo],
    ) -> List[Nautilus.MenuItem]:

        if len(files) > 1:
            item_label = 'Copy Paths'
        else:
            item_label = 'Copy Path'

        main_menuitem = Nautilus.MenuItem(
            name="PathUtils::CopyPath",
            label=item_label,
        )
        main_menuitem.connect('activate', self.__copy_files_path, files)

        return [main_menuitem, ]

    def get_background_items(
            self,
            file: Nautilus.FileInfo,
    ) -> List[Nautilus.MenuItem]:

        # create the main menu item
        main_menuitem = Nautilus.MenuItem(
            name="PathUtils::CopyCurrentDirPath",
            label="Copy Directory Path ...",
        )
        main_menuitem.connect("activate", self.__copy_dir_path, file)

        return [main_menuitem, ]
